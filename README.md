# koSimpleCalculator #

A simple calculator made with knockoutjs.

Demo at [http://4b-labs.com/ben/koSimpleCalculator/](http://4b-labs.com/ben/koSimpleCalculator/)

## Libraries ##
* [https://html5boilerplate.com/](https://html5boilerplate.com/)
* [https://jquery.com/](https://jquery.com/)
* [http://knockoutjs.com/](http://knockoutjs.com/)