// we use this line to initialize without polluting the gloabl window
(function() {

// variable everything is stored
var ns = {};

ns.koSimpleCalculator = function() {
	// assign new instances of koSimpleCalculator to self
	var self = this;

	//
	// what's displayed to the user
	//
	self.display = ko.observable( 0 );

	//
	// the current stored value
	//
	self.memory = ko.observable( 0 );

	//
	// called when the value of self.memory is changed
	// val is the new value of memory
	//
	self.memory.subscribe( function( val ) {
		// change the value displayed to the user
		self.display( val );
	});

	//
	// the following are vars and functions for doing calculations
	//

	//
	// do calculation
	//
	self.calculate = function() {
		// is there a calculation to use
		if ( self.calculation ) {
			// does the calculation method exist
			if ( typeof self[self.calculation] === "function" ) {
				// apply (call) the method
				self.memory( self[self.calculation].apply() );
			}
			// clear the calculation type
			self.calculation = false;
		} else {
			// set memory to display value
			self.memory( self.display() );
		}
		// so we know calculate were just called
		self.calculated = true;
	}

	//
	// calculate function was just called
	// determines if we should replace display with new values
	//
	self.calculated = false;

	//
	// function name to use for calculate
	//
	self.calculation = false;

	//
	// the following are the different functions that are called by calculate
	//

	//
	// addition
	//
	self.calculationAdd = function() {
		return self.memory() + self.display();
	}

	//
	// division
	//
	self.calculationDivide = function() {
		return self.memory() / self.display();
	}

	//
	// multiplication
	//
	self.calculationMultiply = function() {
		return self.memory() * self.display();
	}

	//
	// subtraction
	//
	self.calculationSubtract = function() {
		return self.memory() - self.display();
	}

	//
	// the following are button vars and functions
	//

	//
	// should the next number pressed come after a decimal?
	//
	self.hasDecimal = false;

	//
	// call to append new numbers to the right of display value
	//
	self.appendToDisplay = function( num ) {
		// should we replace or append to the current displayed value?
		if ( self.calculated ) {
			// replace
			var display = "";
		} else {
			// append to
			// get display val
			var display = self.display();
			// change to string
			display = display.toString();
		}
		// check for decimal previously pressed
		if ( self.hasDecimal ) {
			// add . to string
			display = display.concat( "." );
			self.hasDecimal = false;
		}
		// add num to existing display value
		display = display.concat( num.toString() );
		// change display back to a number (floating point)
		display = parseFloat( display );
		// change the value of display
		self.display( display );
	};

	//
	// called when "+" button is clicked
	//
	self.buttonAddPress = function() {
		self.calculate();
		self.calculation = "calculationAdd";
	};

	//
	// called when "clear" button is clicked
	//
	self.buttonClearPress = function() {
		// reset everything
		self.memory( 0 );
		self.display( 0 );
		self.hasDecimal = false;
		self.calculated = false;
		self.calculation = false;
	};

	//
	// called when divide button is clicked
	//
	self.buttonDividePress = function() {
		self.calculate();
		self.calculation = "calculationDivide";
	};

	//
	// called when "=" button is clicked
	//
	self.buttonEqualsPress = function() {
		self.calculate();
		self.display( self.memory() );
		self.calculation = false;
	};

	//
	// called when "x" button is clicked
	//
	self.buttonMultiplyPress = function() {
		self.calculate();
		self.calculation = "calculationMultiply";
	};

	//
	// called when "%" button is clicked
	//
	self.buttonPercentagePress = function() {
		// change to percentage
		self.display( self.display() / 100 );
	};

	//
	// called when "-" button is clicked
	//
	self.buttonSubtractPress = function() {
		self.calculate();
		self.calculation = "calculationSubtract";
	};

	//
	// called when "+/-" button is clicked
	//
	self.buttonSwapPress = function() {
		// switch positive to negative and vice versa
		self.display( self.display() * -1 );
	};

	//
	// called when the "9" button is clicked
	//
	self.buttonNinePress = function() {
		self.appendToDisplay( 9 )
	};

	//
	// called when the "8" button is clicked
	//
	self.buttonEightPress = function() {
		self.appendToDisplay( 8 )
	};

	//
	// called when the "7" button is clicked
	//
	self.buttonSevenPress = function() {
		self.appendToDisplay( 7 )
	};

	//
	// called when the "6" button is clicked
	//
	self.buttonSixPress = function() {
		self.appendToDisplay( 6 )
	};

	//
	// called when the "5" button is clicked
	//
	self.buttonFivePress = function() {
		self.appendToDisplay( 5 )
	};

	//
	// called when the "4" button is clicked
	//
	self.buttonFourPress = function() {
		self.appendToDisplay( 4 )
	};

	//
	// called when the "3" button is clicked
	//
	self.buttonThreePress = function() {
		self.appendToDisplay( 3 )
	};

	//
	// called when the "2" button is clicked
	//
	self.buttonTwoPress = function() {
		self.appendToDisplay( 2 )
	};

	//
	// called when the "1" button is clicked
	//
	self.buttonOnePress = function() {
		self.appendToDisplay( 1 )
	};

	//
	// called when the "0" button is clicked
	//
	self.buttonZeroPress = function() {
		self.appendToDisplay( 0 )
	};

	//
	// called when the "." button is clicked
	//
	self.buttonDecimalPress = function() {
		self.hasDecimal = true;
	};

};

// call when page is fully loaded
$( document ).ready(function() {
	// create a new koSimpleCalculator
	var koSimpleCalculator = new ns.koSimpleCalculator();
	// bind to document
	ko.applyBindings({
		"koSimpleCalculator": koSimpleCalculator
	});
});

}());
